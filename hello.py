import argparse

from textx import metamodel_from_file

parser = argparse.ArgumentParser(
    prog="Hello Interpreter",
)

parser.add_argument("rules")
parser.add_argument("input")

if __name__ == '__main__':
    args = parser.parse_args()

    hello_mm = metamodel_from_file(args.rules)
    hello_model = hello_mm.model_from_file(args.input)
    for who in hello_model.to_greet:
        print(f"Hello, {who.name}{hello_model.end}")
