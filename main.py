from textx import metamodel_from_file

from task2_3.Driver import Driver
from task2_3.program.TextxProgram import TextxProgram

if __name__ == '__main__':
    metamodel = metamodel_from_file('task2_3/grammar/grammar.tx')
    model = metamodel.model_from_file('task2_3/tests/integration/program/maze.karel')

    program: TextxProgram = TextxProgram(model)
    driver: Driver = Driver(program)
    driver.execute()
