import sys
from typing import Optional, Iterable, Any


class AbstractVisitor:
    def __init__(self, output: Optional[str] = None):
        self.output = open(output) if output else sys.stderr

    def __del__(self):
        if self.output:
            self.output.close()

    def run(self, stmt_list: Iterable[Any]):
        for stmt in stmt_list:
            self.visit(stmt)

    def visit(self, unit):
        raise NotImplementedError
