from typing import Optional

from multipledispatch import dispatch

from task2_3.structure.expr.AndExpression import AndExpression
from task2_3.structure.expr.Expression import Expression
from task2_3.structure.expr.OrExpression import OrExpression
from task2_3.structure.stmt.IfStatement import IfStatement
from task2_3.structure.stmt.Statement import Statement
from task2_3.structure.stmt.basic.PrintStatement import PrintStatement
from task2_3.visitor.AbstractVisitor import AbstractVisitor


class PrintVisitor(AbstractVisitor):
    def __init__(self, output: Optional[str] = None):
        super(PrintVisitor, self).__init__(output=output)

    # COMMON

    @dispatch(Statement)
    def visit(self, unit: Statement):
        print(unit.__class__.__name__, file=self.output)

    @dispatch(Expression)
    def visit(self, unit: Expression):
        print(unit.__class__.__name__, file=self.output)

    # CONCRETE

    @dispatch(PrintStatement)
    def visit(self, unit: PrintStatement):
        print(unit.__class__.__name__, file=self.output)
        print(f"text: {unit.text}", file=self.output)

    @dispatch(IfStatement)
    def visit(self, unit: IfStatement):
        print(unit.__class__.__name__, file=self.output)
        print("condition")
        unit.condition.accept(self)
        print("if_stmt_list")
        for stmt in unit.stmt_list:
            stmt.accept(self)
        print("if_stmt_list (end)")

    @dispatch(AndExpression)
    def visit(self, unit: AndExpression):
        print(unit.__class__.__name__, file=self.output)
        unit.lhs.accept(self)
        unit.rhs.accept(self)

    @dispatch(OrExpression)
    def visit(self, unit: OrExpression):
        print(unit.__class__.__name__, file=self.output)
        unit.lhs.accept(self)
        unit.rhs.accept(self)
