from typing import Optional, Any, Iterable

from multipledispatch import dispatch

from task2_3.Driver import Driver
from task2_3.program.Frame import Frame
from task2_3.program.ReturnValue import ReturnValue
from task2_3.structure.expr.AndExpression import AndExpression
from task2_3.structure.expr.CheckExpression import CheckExpression
from task2_3.structure.expr.Expression import Expression
from task2_3.structure.expr.FalseExpression import FalseExpression
from task2_3.structure.expr.FunctionCallExpression import FunctionCallExpression
from task2_3.structure.expr.LvalueExpression import LvalueExpression
from task2_3.structure.expr.NotExpression import NotExpression
from task2_3.structure.expr.OrExpression import OrExpression
from task2_3.structure.expr.TrueExpression import TrueExpression
from task2_3.structure.func.Function import Function
from task2_3.structure.lvalue.Lvalue import Lvalue
from task2_3.structure.stmt.ExprStatement import ExprStatement
from task2_3.structure.stmt.FunctionDecl import FunctionDecl
from task2_3.structure.stmt.IfElseStatement import IfElseStatement
from task2_3.structure.stmt.IfStatement import IfStatement
from task2_3.structure.stmt.IterStatement import IterStatement
from task2_3.structure.stmt.LvalueDeclStatement import LvalueDeclStatement
from task2_3.structure.stmt.LvalueDefStatement import LvalueDefStatement
from task2_3.structure.stmt.ReturnStatement import ReturnStatement
from task2_3.structure.stmt.Statement import Statement
from task2_3.structure.stmt.WhileStatement import WhileStatement
from task2_3.structure.stmt.basic.PrintStatement import PrintStatement
from task2_3.structure.stmt.instruction.Instruction import Instruction
from task2_3.visitor.AbstractVisitor import AbstractVisitor


class Interpreter(AbstractVisitor):
    def __init__(self, driver: Driver, output: Optional[str] = None):
        super(Interpreter, self).__init__(output=output)
        self.driver = driver
        self.frames: list[Frame] = []

    def run(self, stmt_list: Iterable[Statement]):
        self.frames.append(Frame(self, stmt_list))
        self.frames[-1].exec()

    @dispatch(Expression)
    def evaluate(self, expr: Expression):
        expr.accept(self)
        return self.frames[-1].value

    def get_value(self):
        return self.frames[-1].value

    def set_value(self, value: Any):
        self.frames[-1].value = value

    def get_return_value(self):
        return self.frames[-1].return_value

    def set_return_value(self, value: Any):
        self.frames[-1].return_value = ReturnValue(value)

    @dispatch(PrintStatement)
    def visit(self, unit: PrintStatement):
        print(unit.text, file=self.output)

    @dispatch(Instruction)
    def visit(self, unit: Instruction):
        unit.perform()

    @dispatch(IfStatement)
    def visit(self, unit: IfStatement):
        if self.evaluate(unit.condition):
            for stmt in unit.stmt_list:
                stmt.accept(self)
                if self.get_return_value():
                    return

    @dispatch(IfElseStatement)
    def visit(self, unit: IfElseStatement):
        stmt_list = unit.else_stmt_list
        if self.evaluate(unit.condition):
            stmt_list = unit.if_stmt_list

        for stmt in stmt_list:
            stmt.accept(self)
            if self.get_return_value():
                return

    @dispatch(WhileStatement)
    def visit(self, unit: WhileStatement):
        while self.evaluate(unit.condition):
            for stmt in unit.stmt_list:
                stmt.accept(self)
                if self.get_return_value():
                    return

    @dispatch(IterStatement)
    def visit(self, unit: IterStatement):
        for _ in range(unit.times):
            for stmt in unit.stmt_list:
                stmt.accept(self)
                if self.get_return_value():
                    return

    @dispatch(ReturnStatement)
    def visit(self, unit: ReturnStatement):
        if unit.expr:
            self.set_return_value(self.evaluate(unit.expr))
        else:
            self.set_return_value(None)

    @dispatch(ExprStatement)
    def visit(self, unit: ExprStatement):
        self.evaluate(unit.expr)

    @dispatch(FunctionDecl)
    def visit(self, unit: FunctionDecl):
        self.driver.function_registry.add_function(
            Function(name=unit.name,  # args=unit.args,
                     stmt_list=unit.stmt_list)
        )

    @dispatch(LvalueDeclStatement)
    def visit(self, unit: LvalueDeclStatement):
        self.driver.lvalue_registry.add_lvalue(
            Lvalue(type=unit.type, name=unit.name, value=unit.value)
        )

    @dispatch(LvalueDefStatement)
    def visit(self, unit: LvalueDefStatement):
        self.driver.lvalue_registry.update_lvalue(name=unit.name,
                                                  value=unit.value)

    @dispatch(AndExpression)
    def visit(self, unit: AndExpression):
        self.set_value(self.evaluate(unit.lhs) and self.evaluate(unit.rhs))

    @dispatch(OrExpression)
    def visit(self, unit: OrExpression):
        self.set_value(self.evaluate(unit.lhs) or self.evaluate(unit.rhs))

    @dispatch(NotExpression)
    def visit(self, unit: NotExpression):
        self.set_value(not self.evaluate(unit.subexpr))

    @dispatch(TrueExpression)
    def visit(self, unit: TrueExpression):
        self.set_value(True)

    @dispatch(FalseExpression)
    def visit(self, unit: FalseExpression):
        self.set_value(False)

    @dispatch(CheckExpression)
    def visit(self, unit: CheckExpression):
        self.set_value(unit.check())

    @dispatch(LvalueExpression)
    def visit(self, unit: LvalueExpression):
        self.set_value(self.driver.lvalue_registry.get_lvalue(unit.name))

    @dispatch(FunctionCallExpression)
    def visit(self, unit: FunctionCallExpression):
        function = self.driver.function_registry.get_function(unit.name)
        self.run(function.stmt_list)
        return_value = self.get_return_value()
        self.frames.pop()
        self.set_value(return_value.value if return_value else None)
