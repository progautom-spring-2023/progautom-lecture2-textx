from typing import Optional

from task2_3.Robot import Robot
from task2_3.program.AbstractProgram import AbstractProgram
from task2_3.structure.func.FunctionRegistry import FunctionRegistry
from task2_3.structure.lvalue.LvalueRegistry import LvalueRegistry
from task2_3.visitor.PrintVisitor import PrintVisitor


class Driver:
    def __init__(self, program: AbstractProgram):
        self.program: AbstractProgram = program
        self.lvalue_registry: LvalueRegistry = LvalueRegistry()
        self.function_registry: FunctionRegistry = FunctionRegistry()
        self.robot: Robot = Robot()

    def print_ast(self, output: Optional[str] = None):
        visitor: PrintVisitor = PrintVisitor(output)
        self.program.accept(visitor)

    def execute(self, output: Optional[str] = None):
        from task2_3.visitor.Interpreter import Interpreter
        visitor: Interpreter = Interpreter(driver=self, output=output)
        self.program.accept(visitor)
