debug = False

if not debug:
    from karel_robot.run import move, turn_left, turn_right, pick_beeper, put_beeper, front_is_treasure, \
    front_is_blocked, beeper_is_present, facing_north, facing_south, facing_east, facing_west
else:
    print("In debug mode")
    move = lambda: None
    turn_left = lambda: None
    turn_right = lambda: None
    pick_beeper = lambda: None
    put_beeper = lambda: None
    front_is_treasure = lambda: None
    front_is_blocked = lambda: None
    beeper_is_present = lambda: None
    facing_north = lambda: None
    facing_south = lambda: None
    facing_east = lambda: None
    facing_west = lambda: None


class Robot:
    @staticmethod
    def move():
        move()

    @staticmethod
    def turn_left():
        turn_left()

    @staticmethod
    def turn_right():
        turn_right()

    @staticmethod
    def pick_beeper():
        pick_beeper()

    @staticmethod
    def put_beeper():
        put_beeper()

    @staticmethod
    def front_is_treasure():
        return front_is_treasure()

    @staticmethod
    def front_is_blocked():
        return front_is_blocked()

    @staticmethod
    def is_beeper():
        return beeper_is_present()

    @staticmethod
    def north():
        return facing_north()

    @staticmethod
    def south():
        return facing_south()

    @staticmethod
    def east():
        return facing_east()

    @staticmethod
    def west():
        return facing_west()
