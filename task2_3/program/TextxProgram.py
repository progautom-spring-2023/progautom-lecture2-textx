from multipledispatch import dispatch

from task2_3.grammar.Parser import Parser
from task2_3.program.AbstractProgram import AbstractProgram
from task2_3.visitor.AbstractVisitor import AbstractVisitor
from task2_3.visitor.Interpreter import Interpreter


class TextxProgram(AbstractProgram):
    def __init__(self, textx_model):
        self.textx_model = textx_model

    @dispatch(AbstractVisitor)
    def accept(self, visitor: AbstractVisitor):
        parser: Parser = Parser()
        visitor.run(parser.parse(self.textx_model))
