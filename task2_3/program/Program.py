from task2_3.program.AbstractProgram import AbstractProgram
from task2_3.structure.stmt.Statement import Statement
from task2_3.visitor.AbstractVisitor import AbstractVisitor


class Program(AbstractProgram):
    def __init__(self, stmt_list: list[Statement]):
        self.stmt_list: list[Statement] = stmt_list

    def accept(self, visitor: AbstractVisitor):
        for stmt in self.stmt_list:
            visitor.visit(stmt)
