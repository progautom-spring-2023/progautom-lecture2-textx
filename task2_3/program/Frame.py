from typing import Any, Iterable, Optional

from task2_3.program.ReturnValue import ReturnValue
from task2_3.structure.stmt.Statement import Statement
from task2_3.visitor.AbstractVisitor import AbstractVisitor


class Frame:
    def __init__(self, visitor: AbstractVisitor, stmt_list: Iterable[Statement]):
        self.visitor: AbstractVisitor = visitor
        self.stmt_list: Iterable[Statement] = stmt_list
        self.value: Any = None
        self.return_value: Optional[ReturnValue] = None

    def exec(self):
        for stmt in self.stmt_list:
            stmt.accept(self.visitor)
            if self.return_value:
                return
