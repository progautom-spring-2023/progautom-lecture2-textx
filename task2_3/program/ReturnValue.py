from typing import Any


class ReturnValue:
    def __init__(self, value: Any):
        self.value = value
