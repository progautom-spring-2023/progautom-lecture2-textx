from task2_3.visitor.AbstractVisitor import AbstractVisitor


class AbstractProgram:
    def accept(self, visitor: AbstractVisitor):
        raise NotImplementedError
