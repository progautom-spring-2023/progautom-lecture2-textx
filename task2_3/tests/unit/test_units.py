import pytest

from task2_3.structure.expr.AndExpression import AndExpression
from task2_3.structure.expr.FalseExpression import FalseExpression
from task2_3.structure.expr.OrExpression import OrExpression
from task2_3.structure.expr.TrueExpression import TrueExpression
from task2_3.structure.stmt.basic.PrintStatement import PrintStatement
from task2_3.structure.stmt.instruction.BeeperAction import BeeperPutInstruction, BeeperPickInstruction
from task2_3.structure.stmt.instruction.Move import MoveInstruction
from task2_3.structure.stmt.instruction.Turn import TurnLeftInstruction, TurnRightInstruction


@pytest.mark.parametrize('StmtClass, params', [
    [PrintStatement, ["hello, world"]],
])
def test_statement(StmtClass, params):
    statement = StmtClass(*params)
    statement.perform()


@pytest.mark.parametrize('InstructionClass', [
    MoveInstruction,
    TurnLeftInstruction,
    TurnRightInstruction,
    BeeperPickInstruction,
    BeeperPutInstruction,
])
def test_instruction(InstructionClass):
    statement = InstructionClass()
    with pytest.raises(NotImplementedError):
        statement.perform()


@pytest.mark.parametrize('ExprClass, params, expected', [
    [TrueExpression, [], True],
    [FalseExpression, [], False],
    [AndExpression, [TrueExpression(), FalseExpression()], False],
    [OrExpression, [TrueExpression(), FalseExpression()], True],
])
def test_expression(ExprClass, params, expected):
    expression = ExprClass(*params)
    result = expression.check()
    assert result == expected
