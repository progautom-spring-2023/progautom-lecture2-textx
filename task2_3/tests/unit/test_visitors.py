import pytest

from task2_3.Driver import Driver
from task2_3.program.Program import Program
from task2_3.structure.expr.FalseExpression import FalseExpression
from task2_3.structure.expr.TrueExpression import TrueExpression
from task2_3.structure.stmt.basic.AndStatement import AndStatement
from task2_3.structure.stmt.basic.OrStatement import OrStatement
from task2_3.structure.stmt.basic.PrintStatement import PrintStatement
from task2_3.structure.stmt.instruction.BeeperAction import BeeperPutInstruction, BeeperPickInstruction
from task2_3.structure.stmt.instruction.Move import MoveInstruction
from task2_3.structure.stmt.instruction.Turn import TurnLeftInstruction, TurnRightInstruction
from task2_3.visitor.Interpreter import Interpreter
from task2_3.visitor.PrintVisitor import PrintVisitor

contexts = [
    [
        PrintStatement("hello")
    ],
    [
        PrintStatement("hello"),
        PrintStatement("world")
    ],
    [
        AndStatement(TrueExpression(), FalseExpression()),
        OrStatement(TrueExpression(), FalseExpression()),
    ],
    [
        MoveInstruction(),
        TurnLeftInstruction(),
        TurnRightInstruction(),
        BeeperPutInstruction(),
        BeeperPickInstruction(),
    ],
]


@pytest.mark.parametrize('stmt_list', contexts)
def test_print_visitor(stmt_list):
    program = Program(stmt_list)
    visitor = PrintVisitor()
    for stmt in program.stmt_list:
        visitor.visit(stmt)


@pytest.mark.parametrize('stmt_list', contexts)
def test_interpreter(stmt_list):
    program = Program(stmt_list)
    driver = Driver(program)
    visitor = Interpreter(driver)
    for stmt in program.stmt_list:
        visitor.visit(stmt)
