from task2_3.structure.expr.Expression import Expression
from task2_3.structure.stmt.Statement import Statement


class NotExpression(Expression):
    def __init__(self, subexpr: Expression):
        self.subexpr: Expression = subexpr
