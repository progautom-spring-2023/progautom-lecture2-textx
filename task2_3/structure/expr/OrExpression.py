from task2_3.structure.expr.Expression import Expression
from task2_3.structure.stmt.Statement import Statement


class OrExpression(Expression):
    def __init__(self, lhs: Expression, rhs: Expression):
        self.lhs: Expression = lhs
        self.rhs: Expression = rhs
