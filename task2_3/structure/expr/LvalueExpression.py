from task2_3.structure.expr.Expression import Expression
from task2_3.structure.lvalue.Lvalue import Lvalue
from task2_3.structure.stmt.Statement import Statement


class LvalueExpression(Expression):
    def __init__(self, name: str):
        self.name: str = name
