from abc import ABC

from task2_3.Robot import Robot
from task2_3.structure.expr.Expression import Expression


class CheckExpression(Expression, ABC):
    def check(self):
        raise NotImplementedError


class IsBeeperExpression(CheckExpression):
    def check(self):
        return Robot.is_beeper()


class FrontIsTreasureExpression(CheckExpression):
    def check(self):
        return Robot.front_is_treasure()


class FrontIsBlockedExpression(CheckExpression):
    def check(self):
        return Robot.front_is_blocked()


class IsNorthExpression(CheckExpression):
    def check(self):
        return Robot.north()


class IsSouthExpression(CheckExpression):
    def check(self):
        return Robot.south()


class IsEastExpression(CheckExpression):
    def check(self):
        return Robot.east()


class IsWestExpression(CheckExpression):
    def check(self):
        return Robot.west()
