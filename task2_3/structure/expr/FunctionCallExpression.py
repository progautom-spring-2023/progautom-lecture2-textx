from task2_3.structure.expr.Expression import Expression


class FunctionCallExpression(Expression):
    def __init__(self, name: str):
        self.name: str = name
