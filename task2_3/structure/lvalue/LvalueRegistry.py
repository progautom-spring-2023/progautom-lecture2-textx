from typing import Any

from task2_3.structure.lvalue import Lvalue


class LvalueRegistry:
    def __init__(self):
        self.lvalue_list: dict[str, Lvalue] = dict()

    def add_lvalue(self, lvalue: Lvalue):
        self.lvalue_list[lvalue.name] = lvalue

    def get_lvalue(self, name: str):
        assert name in self.lvalue_list
        return self.lvalue_list[name]

    def update_lvalue(self, name: str, value: Any):
        assert name in self.lvalue_list
        self.lvalue_list[name] = value
