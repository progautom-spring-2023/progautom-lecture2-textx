TYPE_TO_CAST_FN = {
    'int': int,
    'float': float,
    'string': str,
    'bool': bool,
}


class Type:
    def __init__(self, name: str):
        assert name in ('int', 'float', 'string', 'bool')
        self.name = name

    @property
    def get_cast_fn(self):
        return TYPE_TO_CAST_FN[self.name]
