from typing import Any, Optional

from task2_3.structure.lvalue.Type import Type


class Lvalue:
    def __init__(self, type: str, name: str, value: Optional[str] = None):
        self.type: Type = Type(name=type)
        self.name: str = name
        self.value: Any = None if value is None else self.type.get_cast_fn(value)
