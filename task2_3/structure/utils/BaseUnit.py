from abc import ABC

from task2_3.visitor.AbstractVisitor import AbstractVisitor


class BaseUnit(ABC):
    def accept(self, visitor: AbstractVisitor):
        visitor.visit(self)
