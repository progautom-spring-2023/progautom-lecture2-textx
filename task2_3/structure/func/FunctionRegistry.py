from task2_3.structure.func.Function import Function


class FunctionRegistry:
    def __init__(self):
        self.function_list: dict[str, Function] = dict()

    def add_function(self, function: Function):
        self.function_list[function.name] = function

    def get_function(self, name: str):
        assert name in self.function_list
        return self.function_list[name]
