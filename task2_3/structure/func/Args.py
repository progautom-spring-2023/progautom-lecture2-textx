from typing import Any

from task2_3.structure.lvalue.Type import Type


class Arg:
    def __init__(self, type: Type, name: str):
        self.type: Type = type
        self.name = name
        self.value = None
