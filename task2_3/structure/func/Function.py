from typing import Optional

from task2_3.structure.func.Args import Arg
from task2_3.structure.stmt.ReturnStatement import ReturnStatement
from task2_3.structure.stmt.Statement import Statement


class Function:
    def __init__(self, name,
                 # args: list[Arg],
                 stmt_list: list[Statement]):
        self.name = name
        # self.args = args
        self.stmt_list = stmt_list
