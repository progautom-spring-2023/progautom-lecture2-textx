from task2_3.structure.expr.Expression import Expression
from task2_3.structure.stmt.Statement import Statement


class IfStatement(Statement):
    def __init__(self, condition: Expression, if_stmt_list: list[Statement]):
        self.condition: Expression = condition
        self.stmt_list: list[Statement] = if_stmt_list
