from typing import Optional

from task2_3.structure.lvalue.Type import Type
from task2_3.structure.stmt.Statement import Statement


class LvalueDeclStatement(Statement):
    def __init__(self, type: str, name: str, value: Optional[str] = None):
        self.type: str = type
        self.name: str = name
        self.value: Optional[str] = value
