from task2_3.structure.expr.Expression import Expression
from task2_3.structure.stmt.Statement import Statement


class IfElseStatement(Statement):
    def __init__(self, condition: Expression, if_stmt_list: list[Statement],
                 else_stmt_list: list[Statement]):
        self.condition: Expression = condition
        self.if_stmt_list: list[Statement] = if_stmt_list
        self.else_stmt_list: list[Statement] = else_stmt_list
