from task2_3.structure.expr.Expression import Expression
from task2_3.structure.stmt.Statement import Statement


class ExprStatement(Statement):
    def __init__(self, expr: Expression):
        self.expr = expr
