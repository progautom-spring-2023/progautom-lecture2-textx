from task2_3.Robot import Robot
from task2_3.structure.stmt.Statement import Statement


class Instruction(Statement):
    def perform(self):
        raise NotImplementedError
