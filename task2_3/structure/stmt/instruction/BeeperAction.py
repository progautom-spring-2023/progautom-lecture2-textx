from abc import ABC

from task2_3.Robot import Robot
from task2_3.structure.stmt.instruction.Instruction import Instruction


class BeeperInstruction(Instruction, ABC):
    pass


class BeeperPickInstruction(BeeperInstruction):
    def perform(self):
        Robot.pick_beeper()


class BeeperPutInstruction(BeeperInstruction):
    def perform(self):
        Robot.put_beeper()
