from abc import ABC

from task2_3.Robot import Robot
from task2_3.structure.stmt.instruction.Instruction import Instruction


class TurnInstruction(Instruction, ABC):
    pass


class TurnLeftInstruction(TurnInstruction):
    def perform(self):
        Robot.turn_left()


class TurnRightInstruction(TurnInstruction):
    def perform(self):
        Robot.turn_right()
