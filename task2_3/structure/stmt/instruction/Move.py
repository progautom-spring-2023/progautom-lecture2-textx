from task2_3.Robot import Robot
from task2_3.structure.stmt.instruction.Instruction import Instruction


class MoveInstruction(Instruction):
    def perform(self):
        Robot.move()
