from task2_3.structure.stmt.instruction.Instruction import Instruction


class ExitInstruction(Instruction):
    def perform(self):
        exit()
