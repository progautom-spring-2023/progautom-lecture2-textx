from task2_3.structure.expr.Expression import Expression
from task2_3.structure.stmt.Statement import Statement


class IterStatement(Statement):
    def __init__(self, times: int, stmt_list: list[Statement]):
        self.times: int = times
        self.stmt_list: list[Statement] = stmt_list
