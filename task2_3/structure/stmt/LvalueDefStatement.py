from typing import Optional

from task2_3.structure.lvalue.Type import Type
from task2_3.structure.stmt.Statement import Statement


class LvalueDefStatement(Statement):
    def __init__(self, name: str, value: Optional[str] = None):
        self.name: str = name
        self.value: Optional[str] = value
