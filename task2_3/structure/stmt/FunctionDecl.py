from typing import Optional

from task2_3.structure.stmt.ReturnStatement import ReturnStatement
from task2_3.structure.stmt.Statement import Statement


class FunctionDecl(Statement):
    def __init__(self, name: str,  # args: list[Arg],
                 stmt_list: list[Statement]):
        self.name: str = name
        # self.args: list[Arg] = args
        self.stmt_list: list[Statement] = stmt_list
