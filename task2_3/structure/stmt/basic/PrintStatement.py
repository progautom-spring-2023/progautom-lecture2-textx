from task2_3.structure.stmt.Statement import Statement


class PrintStatement(Statement):
    def __init__(self, text: str):
        self.text: str = text
