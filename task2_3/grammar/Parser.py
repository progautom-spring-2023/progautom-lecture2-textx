from sys import stderr
from typing import Generator

from task2_3.structure.expr.AndExpression import AndExpression
from task2_3.structure.expr.CheckExpression import FrontIsTreasureExpression, FrontIsBlockedExpression, \
    IsNorthExpression, IsSouthExpression, IsEastExpression, IsWestExpression, IsBeeperExpression
from task2_3.structure.expr.Expression import Expression
from task2_3.structure.expr.FalseExpression import FalseExpression
from task2_3.structure.expr.FunctionCallExpression import FunctionCallExpression
from task2_3.structure.expr.NotExpression import NotExpression
from task2_3.structure.expr.OrExpression import OrExpression
from task2_3.structure.expr.TrueExpression import TrueExpression
from task2_3.structure.stmt.ExprStatement import ExprStatement
from task2_3.structure.stmt.FunctionDecl import FunctionDecl
from task2_3.structure.stmt.IfElseStatement import IfElseStatement
from task2_3.structure.stmt.IfStatement import IfStatement
from task2_3.structure.stmt.IterStatement import IterStatement
from task2_3.structure.stmt.LvalueDeclStatement import LvalueDeclStatement
from task2_3.structure.stmt.LvalueDefStatement import LvalueDefStatement
from task2_3.structure.stmt.ReturnStatement import ReturnStatement
from task2_3.structure.stmt.Statement import Statement
from task2_3.structure.stmt.WhileStatement import WhileStatement
from task2_3.structure.stmt.basic.PrintStatement import PrintStatement
from task2_3.structure.stmt.instruction.BeeperAction import BeeperPickInstruction, BeeperPutInstruction
from task2_3.structure.stmt.instruction.Exit import ExitInstruction
from task2_3.structure.stmt.instruction.Move import MoveInstruction
from task2_3.structure.stmt.instruction.Turn import TurnLeftInstruction, TurnRightInstruction


def textx_isinstance(o, name):
    ans = (o.__class__.__name__ == name)
    # if ans:
    #     print(o, name, file=stderr)
    return ans


class Parser:
    def parse(self, textx_model) -> Generator[Statement, None, None]:
        for stmt in textx_model.func_decl_list:
            yield self.parse_func_decl(stmt)
        for stmt in textx_model.stmt_list:
            yield self.parse_statement(stmt)

    def parse_statement(self, stmt) -> Statement:
        if textx_isinstance(stmt, 'Turn'):
            if stmt.where == 'left':
                return TurnLeftInstruction()
            if stmt.where == 'right':
                return TurnRightInstruction()

        if stmt == 'move':
            return MoveInstruction()
        if stmt == 'exit':
            return ExitInstruction()

        if textx_isinstance(stmt, 'Beeper'):
            if stmt.action == 'pick':
                return BeeperPickInstruction()
            if stmt.action == 'put':
                return BeeperPutInstruction()

        if textx_isinstance(stmt, 'StatementPrint'):
            return PrintStatement(text=stmt.text)

        if textx_isinstance(stmt, 'StatementIf'):
            condition = self.parse_expression(stmt.condition)
            if_stmt_list = [self.parse_statement(substmt)
                            for substmt in stmt.if_stmt_list]
            else_stmt_list = None if not stmt.else_stmt_list else \
                [self.parse_statement(substmt)
                 for substmt in stmt.else_stmt_list]
            if not else_stmt_list:
                return IfStatement(condition=condition,
                                   if_stmt_list=if_stmt_list)
            return IfElseStatement(condition=condition,
                                   if_stmt_list=if_stmt_list,
                                   else_stmt_list=else_stmt_list)

        if textx_isinstance(stmt, 'StatementWhile'):
            condition = self.parse_expression(stmt.condition)
            stmt_list = [self.parse_statement(substmt)
                         for substmt in stmt.stmt_list]
            return WhileStatement(condition=condition, stmt_list=stmt_list)

        if textx_isinstance(stmt, 'StatementIter'):
            stmt_list = [self.parse_statement(substmt)
                         for substmt in stmt.stmt_list]
            return IterStatement(times=stmt.times, stmt_list=stmt_list)

        if textx_isinstance(stmt, 'LvalueDecl'):
            return LvalueDeclStatement(type=stmt.type, name=stmt.name, value=stmt.value)
        if textx_isinstance(stmt, 'LvalueDef'):
            return LvalueDefStatement(name=stmt.name, value=stmt.value)

        if textx_isinstance(stmt, 'StatementReturn'):
            return ReturnStatement(expr=self.parse_expression(stmt.expr))

        return ExprStatement(expr=self.parse_expression(stmt.expr))

    def parse_expression(self, expr) -> Expression:
        if expr == 'True':
            return TrueExpression()
        if expr == 'False':
            return FalseExpression()

        if textx_isinstance(expr, 'Or'):
            if expr.single:
                return self.parse_expression(expr.single)
            lhs = self.parse_expression(expr.left)
            rhs = self.parse_expression(expr.right)
            return OrExpression(lhs, rhs)
        if textx_isinstance(expr, 'And'):
            if expr.single:
                return self.parse_expression(expr.single)
            lhs = self.parse_expression(expr.left)
            rhs = self.parse_expression(expr.right)
            return AndExpression(lhs, rhs)
        if textx_isinstance(expr, 'Not'):
            subexpr = self.parse_expression(expr.subexpr)
            if not expr.is_not:
                return subexpr
            return NotExpression(subexpr)

        if expr == "front_is_treasure":
            return FrontIsTreasureExpression()
        if expr == "front_is_blocked":
            return FrontIsBlockedExpression()

        if expr == "is_beeper":
            return IsBeeperExpression()

        if expr == "north":
            return IsNorthExpression()
        if expr == "south":
            return IsSouthExpression()
        if expr == "east":
            return IsEastExpression()
        if expr == "west":
            return IsWestExpression()

        if textx_isinstance(expr, "FunctionCall"):
            return FunctionCallExpression(name=expr.name)

    def parse_func_decl(self, stmt):
        assert textx_isinstance(stmt, 'FunctionDecl')
        stmt_list = [self.parse_statement(substmt)
                     for substmt in stmt.stmt_list]
        return FunctionDecl(name=stmt.name,
                            stmt_list=stmt_list)

